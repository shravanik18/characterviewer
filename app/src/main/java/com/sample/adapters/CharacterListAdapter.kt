package com.sample.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sample.databinding.ListItemRowBinding
import com.sample.models.RelatedTopic

/**
 * CharacterListAdapter - RecyclerView adapter for displaying a list of RelatedTopic objects.
 */
class CharacterListAdapter(private val onClickListener: OnItemClicked,
                           private val relatedTopic: MutableList<RelatedTopic>) : RecyclerView.Adapter<CharacterListAdapter.CharactersViewHolder>() {

    class CharactersViewHolder(val binding: ListItemRowBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(relatedTopic:RelatedTopic){
            binding.relatedTopic = relatedTopic
            binding.executePendingBindings()
        }

        companion object {
            fun from (parent: ViewGroup): CharactersViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemRowBinding.inflate(layoutInflater,parent,false)
                return CharactersViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharactersViewHolder {
        return CharactersViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return  relatedTopic.size
    }

    override fun onBindViewHolder(holder: CharactersViewHolder, position: Int) {

        val currentTopic:RelatedTopic? = relatedTopic[position]

        // Set Title
        val text = currentTopic?.text
        val regex = Regex("^[^-]+-")
        val matchResult = text?.let { regex.find(it) }
        val title = matchResult?.value?.replace("-", "")?.trim()
        holder.binding.characterTitle.text = title

        holder.binding.characterRow.setOnClickListener{
            if (currentTopic != null) {
                onClickListener.onItemClick(position, currentTopic)
            }
        }

        if (currentTopic != null) {
            holder.bind(currentTopic)
        }
    }


    fun addAll(topicsList: List<RelatedTopic>){
        relatedTopic.clear()
        this.relatedTopic += topicsList
        notifyDataSetChanged()
    }


    interface OnItemClicked {
        fun onItemClick(position: Int, relatedTopic: RelatedTopic)
    }


}