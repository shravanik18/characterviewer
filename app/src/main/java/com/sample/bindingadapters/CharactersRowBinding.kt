package com.sample.bindingadapters

import android.util.Log
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import com.sample.ui.fragments.ListFragmentDirections

class CharactersRowBinding {

    companion object {

        @BindingAdapter("onCharacterRowClickListener")
        @JvmStatic
        fun onCharacterRowClickListener(rowLayout:ConstraintLayout , relatedTopic:com.sample.models.RelatedTopic){
            rowLayout.setOnClickListener{
                try {

                    val action = ListFragmentDirections.actionListFragmentToDetailFragment(relatedTopic)
                    rowLayout.findNavController().navigate(action)

                }catch (e:Exception){
                    Log.d("onRowClickListener", e.toString())
                }
            }
        }

    }
}