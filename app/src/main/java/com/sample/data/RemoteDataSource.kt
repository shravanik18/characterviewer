package com.sample.data

import com.sample.data.network.CharactersApiService
import retrofit2.Response
import javax.inject.Inject

/**
 * Class for making HTTP requests to a remote server and retrieving a list of character data.
 *
 * This class uses an instance of the CharactersApiService interface to make the HTTP request.
 * The "getCharacters" method takes in two parameters "queries" and "format" which are used to construct
 * the query parameters of the HTTP request.
 *
 */
class RemoteDataSource @Inject constructor(
    private val charactersApiService: CharactersApiService
) {

    suspend fun getCharacters(queries:String, format:String) : Response<com.sample.models.Character>{
        return charactersApiService.getCharacters(queries,format)
    }
}