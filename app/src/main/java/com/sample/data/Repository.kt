package com.sample.data

import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

/**
 * Class for providing access to the remote data source through an instance of the RemoteDataSource class.
 *
 *  TODO Local data source
 *
 * This class is annotated with "@ActivityRetainedScoped", which indicates that the instance of this class will
 * be retained for the lifetime of the activity that uses it.
 *
 */
@ActivityRetainedScoped
class Repository @Inject constructor(remoteDataSource: RemoteDataSource){
    val remote = remoteDataSource
}