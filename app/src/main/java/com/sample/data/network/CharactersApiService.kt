package com.sample.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import com.sample.models.Character as SCharacter

/**
 * Interface for making HTTP requests to the characters API.
 */
interface CharactersApiService {

    /**
     * GetCharactersList  - provides a list of character data that can be filtered and formatted using query parameters.
     */
    @GET("/")
    suspend fun getCharacters(@Query("q") q: String, @Query("format") format: String): Response<SCharacter>

}