package com.sample.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Character(

    @SerializedName("RelatedTopics")
    var relatedTopics: List<RelatedTopic>?,

):Parcelable
