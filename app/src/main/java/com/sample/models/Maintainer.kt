package com.sample.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Maintainer(
    @SerializedName("github")
    val github: String
):Parcelable