package com.sample.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class Meta(

    @SerializedName("attribution")
    val attribution: @RawValue Any?,
    @SerializedName("blockgroup")
    val blockgroup: @RawValue Any,
    @SerializedName("created_date")
    val created_date: @RawValue Any,
    @SerializedName("description")
    val description: String?,
    @SerializedName("designer")
    val designer: @RawValue Any,
    @SerializedName("dev_date")
    val dev_date: @RawValue Any,
    @SerializedName("dev_milestone")
    val dev_milestone: String?,
    @SerializedName("developer")
    val developer: List<Developer>,
    @SerializedName("example_query")
    val example_query: String,
    @SerializedName("id")
    val id: String?,
    @SerializedName("is_stackexchange")
    val is_stackexchange:@RawValue  Any,
    @SerializedName("js_callback_name")
    val js_callback_name: String,
    @SerializedName("live_date")
    val live_date: @RawValue Any,
    @SerializedName("maintainer")
    val maintainer: Maintainer,
    @SerializedName("name")
    val name: String,
    @SerializedName("perl_module")
    val perl_module: String,
    @SerializedName("producer")
    val producer: @RawValue Any,
    @SerializedName("production_state")
    val production_state: String,
    @SerializedName("repo")
    val repo: String,
    @SerializedName("signal_from")
    val signal_from: String,
    @SerializedName("src_domain")
    val src_domain: String,
    @SerializedName("src_id")
    val src_id: Int,
    @SerializedName("src_name")
    val src_name: String,
    @SerializedName("src_options")
    val src_options: SRCOptions,
    @SerializedName("src_url")
    val src_url: @RawValue Any,
    @SerializedName("status")
    val status: String,
    @SerializedName("tab")
    val tab: String,
    @SerializedName("topic")
    val topic: List<String>,
    @SerializedName("unsafe")
    val unsafe: Int

):Parcelable