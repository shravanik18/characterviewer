package com.sample.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RelatedTopic(

    @SerializedName("FirstURL")
    val firstUrl: String?,

    @SerializedName("Result")
    var result: String?,

    @SerializedName("Text")
    var text: String?,

    @SerializedName("Icon")
    val icon:Icon?
) : Parcelable
