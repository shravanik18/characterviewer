package com.sample.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SRCOptions(

    @SerializedName("directory")
    val directory: String,
    @SerializedName("is_fanon")
    val is_fanon: Int,
    @SerializedName("is_mediawiki")
    val is_mediawiki: Int,
    @SerializedName("is_wikipedia")
    val is_wikipedia: Int,
    @SerializedName("language")
    val language: String,
    @SerializedName("min_abstract_length")
    val min_abstract_length: String,
    @SerializedName("skip_abstract")
    val skip_abstract: Int,
    @SerializedName("skip_abstract_paren")
    val skip_abstract_paren: Int,
    @SerializedName("skip_end")
    val skip_end: String,
    @SerializedName("skip_icon")
    val skip_icon: Int,
    @SerializedName("skip_image_name")
    val skip_image_name: Int,
    @SerializedName("skip_qr")
    val skip_qr: String,
    @SerializedName("source_skip")
    val source_skip: String,
    @SerializedName("src_info")
    val src_info: String
):Parcelable