package com.sample.ui.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import coil.load
import com.sample.R
import com.sample.databinding.FragmentDetailBinding
import com.sample.models.RelatedTopic
import com.sample.ui.MainActivity
import com.sample.util.Constants
import com.sample.util.Constants.Companion.KEY_RELATED_TOPIC
import com.sample.viewmodels.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Detail fragment - displaying the details of the item clicked in the ListFragment
 */
@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var _binding : FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private lateinit var detailViewModel:DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        detailViewModel = ViewModelProvider(requireActivity())[DetailViewModel::class.java]
        detailViewModel.relatedTopic.observe(this,mRelatedTopicObserver)
    }

    override fun onResume() {
        super.onResume()

        if( !resources.getBoolean(R.bool.isTablet)) {
            (requireActivity() as MainActivity).supportActionBar?.title = "Details"
        }
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentDetailBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(arguments == null){
            return
        }

        var relatedTopic: RelatedTopic? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requireArguments().getParcelable(KEY_RELATED_TOPIC, RelatedTopic::class.java)
        } else{
            requireArguments()!!.getParcelable(KEY_RELATED_TOPIC)
        }

        if (relatedTopic == null) {
            return
        }
        this.detailViewModel.setSelectedRelatedTopic(relatedTopic)
    }

    private val mRelatedTopicObserver = Observer<RelatedTopic>{relatedTopic ->

        if(relatedTopic == null){
            // Show message no data available
            return@Observer
        }
        updateRelatedTopicData(relatedTopic)
    }

    private fun updateRelatedTopicData(relatedTopic:RelatedTopic){
        if (relatedTopic != null) {

            // Title
            val text = relatedTopic.text
            val regex = Regex("^[^-]+-")
            val matchResult = text?.let { regex.find(it) }
            val title = matchResult?.value?.replace("-", "")?.trim()
            binding.detailTitle.text = title

            // Image
            val iconUrl: String? = relatedTopic.icon?.url
            if (relatedTopic.icon != null && relatedTopic.icon!!.url != null && relatedTopic.icon!!.url != "") {
                val imageUrl: String = Constants.BASE_IMAGE_URL + iconUrl
                binding.detailCharacterImage.load(imageUrl)
            } else {
                binding.detailCharacterImage.load(R.drawable.ic_android)
            }

            /*if(resources.getBoolean(R.bool.isSimpsonsMode)){
                binding.detailCharacterImage.scaleType = ImageView.ScaleType.FIT_CENTER
            }else{
                binding.detailCharacterImage.scaleType = ImageView.ScaleType.CENTER_CROP
            }*/


            // Summary Text
            binding.summaryTextView.text = relatedTopic.text
        }
    }
}