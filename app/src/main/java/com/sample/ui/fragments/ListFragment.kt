package com.sample.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.sample.R
import com.sample.adapters.CharacterListAdapter
import com.sample.databinding.FragmentListBinding
import com.sample.models.RelatedTopic
import com.sample.ui.MainActivity
import com.sample.util.Constants.Companion.FORMAT
import com.sample.util.NetworkResult
import com.sample.viewmodels.CharacterListViewModel
import com.sample.viewmodels.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * List fragment - displays list of characters
 */
@AndroidEntryPoint
class ListFragment : Fragment(),CharacterListAdapter.OnItemClicked, SearchView.OnQueryTextListener,SearchView.OnCloseListener  {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!
    private var searchView :SearchView? = null
    private var detailFragment:DetailFragment? = null
    private var relatedTopics : List<RelatedTopic>? = null
    private lateinit var characterListViewModel: CharacterListViewModel
    private lateinit var detailViewModel:DetailViewModel

    private val mAdapter:CharacterListAdapter by lazy { CharacterListAdapter(this, ArrayList()) } // Lazy initialization

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Init
        characterListViewModel = ViewModelProvider(requireActivity())[CharacterListViewModel::class.java]
        detailViewModel = ViewModelProvider(requireActivity())[DetailViewModel::class.java]
    }

    override fun onResume() {
        super.onResume()

        // Set Action bar title
        (requireActivity() as MainActivity).supportActionBar?.title = "Characters"


        // If tablet mode
        // Initiate detail fragment to be viewed on right pane of screen
        if (binding.detailFragmentContainer != null) {
            detailFragment = DetailFragment()
            childFragmentManager.beginTransaction()
                .replace(binding.detailFragmentContainer!!.id, detailFragment!!)
                .commit()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        if(_binding == null) {
            _binding = FragmentListBinding.inflate(inflater, container, false)
            binding.lifecycleOwner = this
            binding.characterListViewModel = characterListViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup recycler view
        binding.listRecyclerview.layoutManager = LinearLayoutManager(requireContext())
        binding.listRecyclerview.adapter = mAdapter

        // Add options menu
        val menuHost : MenuHost = requireActivity()
        menuHost.addMenuProvider(object: MenuProvider {

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.search_menu,menu)

                var searchMenu = menu.findItem(R.id.menu_search)
                searchView = searchMenu.actionView as? SearchView
                searchView?.isSubmitButtonEnabled = true
                searchView?.setOnQueryTextListener(this@ListFragment)
                searchView?.setOnCloseListener(this@ListFragment)


                searchView?.isSubmitButtonEnabled = true


                if(characterListViewModel.isInSearchMode.value == true){
                    val q = characterListViewModel.searchText.value
                    Log.i("ListFragment", " search query is: ~2~~~~~~ ${characterListViewModel.searchText.value}")

                    searchView!!.isIconified = false // expand the search view
                    searchView!!.setQuery(q, false) // set the search query text
                    searchView!!.clearFocus()
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return true
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)

        // Observe filtered List
        observeFilteredItems()


        // Get Data from API
        val characterResponse = characterListViewModel.characterResponse
        if(characterResponse.value != null){

            if(characterListViewModel.isInSearchMode.value == false){
                getDataFromAPI(false)
            }
        }else {
            getDataFromAPI(true)
        }
    }

    /**
     *  Get Character List from API
     *  Check if the viewModel already has content,
     *  If viewModel does not have content - get content from API
     *  If search was performed, load list related to filtered items
     *  otherwise load regular list
     */
    private fun getDataFromAPI( getFromAPI :Boolean){

        if(getFromAPI) {
            val query = resources.getString(R.string.query_characters)
            characterListViewModel.getCharacter(query, FORMAT)
        }

        characterListViewModel.characterResponse.observe(viewLifecycleOwner){response ->

            when (response) {
                is NetworkResult.Success -> {
                    response.data?.let { character ->
                        relatedTopics = character.relatedTopics
                        if (relatedTopics != null) {


                            val q = characterListViewModel.searchText.value
                            if (q != null) { // If search text available, get filtered items
                                characterListViewModel.getFilteredItems(q,relatedTopics!!)
                            }
                            else{
                                // Set Data to adapter list
                                mAdapter.addAll(relatedTopics!!)

                                // Save selected position as 1st position initially
                                characterListViewModel.setSelectedPosition(0)

                                // for tablet mode
                                // Show 1st item on the right pane by default
                                if (binding.detailFragmentContainer != null) {

                                    if (characterListViewModel.selectedPosition.value != null) {
                                        val pos = characterListViewModel.selectedPosition.value
                                        val relatedTopic = relatedTopics!![pos!!]
                                        detailViewModel.setSelectedRelatedTopic(relatedTopic)
                                    }
                                }
                            }
                        }
                    }
                }
                is NetworkResult.Error -> {
                    Toast.makeText(requireContext(),response.message.toString(),Toast.LENGTH_SHORT).show()
                }
                else -> {

                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    /**
     *
     */
    override fun onItemClick(position: Int, relatedTopic: RelatedTopic) {

        // save current selected item to viewModel; can be used to show selected item in tablet mode
        characterListViewModel.setSelectedPosition(position)

        // when a topic is clicked in the list
        //
        // If tablet mode - set clicked topic in detailViewModel  an observe changes in Detail Fragment
        // If Phone mode - navigate to Detail fragment and pass related topic through nav args
        if(binding.detailFragmentContainer != null){
            this.detailViewModel.setSelectedRelatedTopic(relatedTopic)
        }
        else{
            val action = relatedTopic.let { ListFragmentDirections.actionListFragmentToDetailFragment(it) }
            findNavController().navigate(action)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if(query != null){

            characterListViewModel.getFilteredItems(query, relatedTopics!!)
            characterListViewModel.setSearchMode(true)
            characterListViewModel.setSearchText(query)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText == null || newText.isEmpty()){

            // Reset the list to main list when the search text is cleared
            resetSearchList()
        }
        return true
    }

    override fun onClose(): Boolean {
        resetSearchList()
        searchView?.clearFocus()
        getDataFromAPI(false)
        return true
    }

    private fun resetSearchList(){
        characterListViewModel.setSearchText("")
        characterListViewModel.setSearchMode(false)
        mAdapter.notifyDataSetChanged()
    }

    private fun observeFilteredItems(){
        characterListViewModel.filteredItems.observe(viewLifecycleOwner){filteredList->
            mAdapter.addAll(filteredList)
            val relatedTopic = filteredList[0]
            detailViewModel.setSelectedRelatedTopic(relatedTopic)
        }
    }
}