package com.sample.util

class Constants {

    companion object{

        const val BASE_URL = "http://api.duckduckgo.com"
        const val BASE_IMAGE_URL = "https://duckduckgo.com"
        const val FORMAT = "json"

        const val KEY_RELATED_TOPIC = "relatedTopic"
    }

}