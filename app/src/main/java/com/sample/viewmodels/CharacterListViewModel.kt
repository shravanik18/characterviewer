package com.sample.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sample.data.Repository
import com.sample.models.Character
import com.sample.models.RelatedTopic
import com.sample.util.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

/**
 * CharacterListViewModel - ViewModel for List Fragment
 */
@HiltViewModel
class CharacterListViewModel @Inject constructor (
    private val repository: Repository, // Injecting Repository
    application: Application,
) : AndroidViewModel(application) {

    /** Holds the current position of selected character **/
    private val _selectedPosition = MutableLiveData<Int>()
    val selectedPosition: LiveData<Int> get() = _selectedPosition

    fun setSelectedPosition(position: Int) {
        _selectedPosition.value = position
    }

    private val _searchText = MutableLiveData<String>()
    val searchText: LiveData<String> get() = _searchText

    fun setSearchText(text: String) {
        _searchText.value = text
    }

    private val _isInSearchMode = MutableLiveData<Boolean>()
    val isInSearchMode: LiveData<Boolean> get() = _isInSearchMode

    fun setSearchMode(mode: Boolean) {
        _isInSearchMode.value = mode
    }

    private val _filteredItems : MutableLiveData<List<RelatedTopic>> = MutableLiveData()
    val filteredItems : LiveData<List<RelatedTopic>> get() = _filteredItems

    private fun setFilteredItems(items:List<RelatedTopic>){
        _filteredItems.value = items
    }

    fun getFilteredItems(query:String, items:List<RelatedTopic>){
        filterItems(query, items)
    }


    /** RETROFIT **/
    private val _characterResponse : MutableLiveData<NetworkResult<Character>> = MutableLiveData()
    val characterResponse : LiveData<NetworkResult<Character>> get() = _characterResponse

    private fun setCharacterResponse(character: NetworkResult<Character>){
        _characterResponse.value = character
    }

    fun getCharacter(queries: String, format:String) = viewModelScope.launch {
        getCharactersSafeCall(queries, format)
    }

    private suspend fun getCharactersSafeCall(queries: String, format:String){
        setCharacterResponse(NetworkResult.Loading())
        if(hasInternetConnection()){
            try{
                val response = repository.remote.getCharacters(queries,format)
                handleCharactersResponse(response)?.let {
                    setCharacterResponse(it)
                }
            }catch (e:Exception){
                setCharacterResponse(NetworkResult.Error("Characters not found", null))
            }
        }
        else{
            setCharacterResponse(NetworkResult.Error("No Internet Connection", null))
        }
    }

    private fun handleCharactersResponse(response: Response<Character>): NetworkResult<Character>? {
        setCharacterResponse(NetworkResult.Loading())

        return when {
            response.message().toString().contains("timeout") -> {
                NetworkResult.Error("Timeout", null)
            }
            response.body()!!.relatedTopics.isNullOrEmpty() ->{
                NetworkResult.Error("Characters Not Found", null)
            }
            response.isSuccessful -> {
                val characters = response.body()
                NetworkResult.Success(characters!!)
            }
            else ->{
                NetworkResult.Error(response.message(), null)
            }
        }
    }

    private fun filterItems(query:String, items: List<RelatedTopic>){
        if(items.isNotEmpty()){
            setSearchMode(true)
            val newList = items.filter {it->
                it.text?.contains(query.toString(), true) == true
            }
            setFilteredItems(newList)
        }else{
            setSearchMode(false)
        }
    }

    /**
     * Function will return true if internet is available; false otherwise
     */
    private fun hasInternetConnection() : Boolean {

        val connectivityManager = getApplication<Application>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}