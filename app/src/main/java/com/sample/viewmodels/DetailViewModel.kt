package com.sample.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sample.models.RelatedTopic
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * DetailViewModel - ViewModel for Detail Fragment
 */
@HiltViewModel
class DetailViewModel @Inject constructor (application: Application) : BaseViewModel(application){

    private val _relatedTopic = MutableLiveData<RelatedTopic>()
    val relatedTopic: LiveData<RelatedTopic> get() = _relatedTopic

    fun setSelectedRelatedTopic(relatedTopic: RelatedTopic) {
        _relatedTopic.value = relatedTopic
    }
}