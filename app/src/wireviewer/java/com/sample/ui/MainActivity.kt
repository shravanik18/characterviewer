package com.sample.ui

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.sample.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private var isTabletMode: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isTabletMode = resources.getBoolean(R.bool.isTablet)

        if(!isTabletMode){
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
            val  navController:NavController = navHostFragment.navController
            NavigationUI.setupActionBarWithNavController(this,navController)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        if(!isTabletMode) {
            val navController = Navigation.findNavController(this, R.id.navHostFragment);
            return navController.navigateUp() || super.onSupportNavigateUp()
        }
        else{
            return true
        }
    }

}